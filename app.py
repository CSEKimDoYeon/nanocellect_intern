from flask import Flask, render_template, request
import plotly
import plotly.graph_objs as go

import json
import db

app = Flask(__name__)


@app.route('/')
def index():
    feature = 'case1'
    bar = create_plot(feature)  # The initial page is floated in bar type.
    return render_template('index.html', plot=bar)


def create_plot(feature):
    engine_base = db.engine

    if feature == 'case1':
        df = db.extract_Underfifty(engine_base)  # creating a sample dataframe
        print(df)
        data = [
            go.Bar(
                x=df['company'],  # assign x as the dataframe column 'x'
                y=df['count'],
                marker={
                    "color": 'rgb(240,26,26)'
                },
                text=df['count'],
                textposition="outside",
                hoverinfo='none'
            )
        ]
    elif feature == 'case2':
        df_1 = db.extract_Passcase(engine_base)
        df_2 = db.extract_Failcase(engine_base)

        trace1 = {
            "x": df_1['actionType'].tolist(),
            "y": df_1['count'].tolist(),
            "name": 'Pass',
            "type": 'bar',
            "marker": {
                "color": 'rgb(142,124,195)'
            },
            "text" : df_1['count'].tolist(),
            "textposition" : "outside",
            "hoverinfo" : 'none'
        }

        trace2 = {
            "x": df_2['actionType'].tolist(),
            "y": df_2['count'].tolist(),
            "name": 'Fail',
            "type": 'bar',
            "text": df_2['count'].tolist(),
            "textposition": "outside",
            "hoverinfo": 'none'
        }

        data = [trace1, trace2]
        print("result : ", data)

    elif feature == 'case3':
        df = db.extract_Avgover(engine_base)
        print(df)
        data = [
            go.Bar(
                x=df['cartridgeType'],  # assign x as the dataframe column 'x'
                y=df['count'],
                marker={
                    "color": 'rgb(26,40,237)'
                },
                text=df['count'],
                textposition="outside",
                hoverinfo='none'
            )
        ]


    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    print("graphJSON : ", graphJSON)

    return graphJSON


@app.route('/bar', methods=['GET', 'POST'])
def change_features():
    feature = request.args['selected']
    graphJSON = create_plot(feature)

    return graphJSON


if __name__ == '__main__':
    app.run()
