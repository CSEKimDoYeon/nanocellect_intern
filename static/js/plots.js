$('#first_cat').on('change', function () {

    $.ajax({
        url: "/bar",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        data: {
            'selected': document.getElementById('first_cat').value

        },
        dataType: "json",
        success: function (data) {

            var layout = {
                title: "DashBoard",
                autosize: false,
                width: 500,
                height: 500,
                margin: {
                    l: 50,
                    r: 50,
                    b: 100,
                    t: 100,
                    pad: 4
                },
                paper_bgcolor: '#fff965',
                plot_bgcolor: '#ffffff',

                yaxis: {
                    autotick: false,
                    ticks: 'outside',
                    tick0: 0,
                    dtick: 1,
                    ticklen: 8,
                    tickwidth: 4,
                    tickcolor: '#000'
                }
            };

            Plotly.newPlot('bargraph', data, layout);
            // 'Bargraph' is the ID of div that will float graphically.
        }
    });
})
