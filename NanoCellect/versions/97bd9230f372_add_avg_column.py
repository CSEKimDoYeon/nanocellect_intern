"""add AVG column

Revision ID: 97bd9230f372
Revises: 
Create Date: 2019-08-01 14:48:59.314457

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '97bd9230f372'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""ALTER TABLE \"demo\" ADD \"avg\" INTEGER""")
    op.execute("""UPDATE \"demo\" SET \"avg\" = (\"A\" + \"B\" + \"C\")/3 """)
    pass


def downgrade():
    op.execute("""ALTER TABLE \"demo\" DROP \"avg\"""")
    pass
