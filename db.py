
from sqlalchemy import create_engine
import pandas as pd

def extract_Underfifty(engine) :
    connection = engine.connect() # db connect
    df = pd.read_sql_query("select company, count(*) from \"demo\" where \"A\"<50 and \"B\"<50 and \"C\"<50 group by company", engine)

    connection.close() # db disconnect
    return df

def extract_Passcase(engine) :
    connection = engine.connect() # db connect
    df = pd.read_sql_query(
        "select \"actionType\", count(*) from \"demo\" where \"testResult\" like \'pass\' group by \"actionType\"", engine)

    connection.close() # db disconnect
    return df

def extract_Failcase(engine) :
    connection = engine.connect() # db connect
    df = pd.read_sql_query(
        "select \"actionType\", count(*) from \"demo\" where \"testResult\" like \'fail\' group by \"actionType\"", engine)

    connection.close() # db disconnect
    return df

def extract_Avgover(engine) :
    connection = engine.connect() # db connect
    df = pd.read_sql_query(
        "select \"cartridgeType\", count(*) from demo where \"avg\">70 group by \"cartridgeType\"", engine)

    connection.close() # db disconnect
    return df

engine = create_engine('postgresql+psycopg2://postgres:rnsan-4226@localhost/postgres')
connection = engine.connect()
result = connection.execute("select * from demo")


df_base = pd.read_sql("select * from demo", connection)


connection.close()
extract_Underfifty(engine)


